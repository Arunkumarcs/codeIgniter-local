const Controller = require('./Controller');

/** 
 * Add below html in footer oh page.
 * 
 *  <div id="ak-loader">
 *      <img src="/images/ak-loader.gif" style="width:150px;">
 *  </div>
 */
class _Loader extends Controller {
    _properties(self) {
        self.element = $('#ak-loader');

        self.show = (callback = () => {}) => {
            self.element.fadeIn('slow', callback);
        };
        self.hide = (callback = () => {}) => {
            self.element.fadeOut('slow', callback);
        };
    }

    _init(self) {
        self.e_windows(self);
    }

    e_windows(self) {
        $(window).on('load', () => {
            setTimeout(function() {
                self.element.fadeOut('slow');
            }, 200);
        });

        return self;
    }
}

module.exports = new _Loader();