class _Controller {
    constructor(pageName = '') {
        this._pageName = pageName;
        this._properties(this);
        this._init(this);
    }

    _properties(self) {}
    _init(self) {}
}

module.exports = _Controller;