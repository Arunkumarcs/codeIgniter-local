<?php 
require_once __DIR__.'/Model.php';

class Category extends Model {

    // Details
    protected $_Name = 'category';
    protected $_Id   = 'cid';

    // fields
    public $cid;
    public $category_name;

    public function catlist()
    {
        $query = $this->db->get($this->_Name);
        return $query->result();
    }
}