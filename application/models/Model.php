<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model extends CI_Model 
{
    // Details
    protected $_Name = '';
    protected $_Id   = '';

    // fields
    // public $cid; .. 

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->_init();
    }
    
    protected function _init() { }
    
    /**
     * Fetch all resultant select rows.
     */
    final public function rows($query)
    {
        $return = array();
        foreach ($query->result_array() as $row) {
            $return[] = $row;
        }
        $query->free_result();
        return $return;
    }
    
    /**
     * Get
     * - select, where, limit, order_by, join, group_by, having
     */
    final public function get($data = array(), $debug = false) 
    {
        // Select
        if(isset($data['select']) && $data['select'] != '') {
            $this->db->select($data['select']);
        } else {
            $this->db->select('*');
        }

        // Where - (array, string)
        if(isset($data['where'])) {
            $this->db->where($data['where']);
        }
        
        // Group By - (array)
        if(isset($data['group_by'])) {
            $this->db->group_by($data['group_by']);
        }
        
        // Having - (array)
        if(isset($data['having'])) {
            $this->db->having($data['where']);
        }
    
        // Order By
        if(isset($data['order_by']) && is_array($data['order_by'])) {
            foreach ($data['order_by'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        } else if(isset($data['order_by'])){
            $this->db->order_by($data['order_by']);
        }

        // Limit
        if(isset($data['limit']) && isset($data['count'])) {
            $this->db->limit($data['limit'], $data['count']);
        } else if(isset($data['limit'])) {
            $this->db->limit($data['limit']);
        }

        // Query
        $query = $this->db->get_compiled_select($this->_Name);
        if($debug) {
            dump($query);
        }
        $query = $this->db->query($query);
        return $this->rows($query);
    }

    /**
     * Insert
     */
    final public function set($data = array(), $where = '', $debug = false)
    {
        if($where != '') {
            $this->db->where($where);
            $sql = $this->db->get_compiled_update($this->_Name, $data);
            
            if($debug) {
                dump($sql);
            }
            return $this->db->query($query);
        } else {
            $sql = $this->db->set($data)->get_compiled_insert($this->_Name);
            
            if($debug) {
                dump($sql);
            }
            $this->db->query($query);
            return $this->db->insert_id();
        }
    }

    /**
     * Delete
     */
    public function unset($where)
    {
        if( isset($where) 
            && (
                (is_array($where) && count($where) > 0) 
                || $where != ''
            )
        ) {
            $this->db->where($where);
            $this->db->delete($tables);
        }
    }
}