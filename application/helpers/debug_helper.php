<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(! function_exists('dump')) {
    function dump($param)
    {
        if(ENVIRONMENT != 'production') {
            echo "<pre>";
            print_r($param);
            echo "</pre>";
        }
    }
}