<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(! function_exists('dateDifference')) {
    function dateDifference($from, $to = '')
    {
        if($to == '') {
            $to = date('Y-m-d H:i:s', time());
            dump($to);
            dump($from);
        }

        $dateFrom = new DateTime($from); 
        $dateTo   = new DateTime($to); 
        return round(($dateTo->getTimestamp() - $dateFrom->getTimestamp())/60);
    }
}
