<?php
require_once __DIR__.'/Lib.php';

class CsvReader extends Lib
{
    
    final public function readTitle($file)
    {
        $file = new SplFileObject($file['tmp_name']);
        $reader = new \Ddeboer\DataImport\Reader\CsvReader($file);
        
        foreach ($reader as $key => $value) {
            return $value;
        }
    }
    
    final public function readFields($file)
    {
        $return = array();
        $file   = new SplFileObject($file['tmp_name']);
        $reader = new \Ddeboer\DataImport\Reader\CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $key => $value) {
            $return[] = $value;
        }
        return $return;
    }

    final public function arrayToCsvDownload(
        $array,
        $filename = "export.csv",
        $delimiter=","
    ) {
        // open raw memory as file so no temp files needed, you might run out of memory though
        $f = fopen('php://memory', 'w');
        // loop over the input array
        foreach ($array as $line) {
            // generate csv lines from the inner arrays
            fputcsv($f, $line, $delimiter);
        }
        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        // make php send the generated csv lines to the browser
        fpassthru($f);
    }
}