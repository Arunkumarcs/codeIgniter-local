<?php
require_once __DIR__.'/Lib.php';

class AuEncrypt extends Lib
{
    
    protected function _init() {
        $this->CI->config->load('keys');
    }

    public function encrypt($value)
    {
        return $this->encrypt_decrypt('encrypt', $value);
    }

    public function decrypt($value)
    {
        return $this->encrypt_decrypt('decrypt', $value);
    }
        
    private function encrypt_decrypt($action, $string) {
        $pass_keys      = $this->CI->config->item('password');
        $encrypt_method = $pass_keys['method'];
        $secret_key     = $pass_keys['key'];
        $secret_iv      = $pass_keys['iv'];
        $output         = false;
        
        // hash
        // $key = hash('sha256', $secret_key);
        // $iv  = substr(hash('sha256', $secret_iv), 0, 16);
        $key = $secret_key;
        $iv  = $secret_iv;

        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }
}
