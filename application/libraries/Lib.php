<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Lib 
{
    protected $CI;

    public function __construct()
    { 
        $this->CI =& get_instance();
        call_user_func_array(array($this, '_init'), func_get_args());
    }

    protected function _init() {}
}