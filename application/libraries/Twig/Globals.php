<?php
require_once dirname(__DIR__).'/Lib.php';

class Globals extends Lib
{
    public function session($var = '')
    {
        if($var == '') {
            return $this->CI->session->userdata();
        } else {
            return $this->CI->session->userdata($var);
        }
    }
}
