<?php
require_once __DIR__.'/Lib.php';
require_once __DIR__.'/Twig/Globals.php';

class Twig extends Lib
{
    public function globals()
    {
        return new Globals();
    }
}