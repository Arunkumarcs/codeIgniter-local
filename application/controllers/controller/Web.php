<?php
use DebugBar\StandardDebugBar;
require_once __DIR__.'/Controller.php';

class Web extends Controller 
{
    protected $_theme          = 'Default/';
    protected $_debug          = null;
    
    private $_twig             = null;
    private $_debugbarRenderer = null;
    
	public function __construct()
	{
        parent::__construct();
        $this->load->library('session');
        
        // Debugbar
        if($this->config->item('DebugBar')) { 
            $config = $this->config->config;
            $this->_debug = new StandardDebugBar();
            $this->_debug->addCollector(
                new DebugBar\DataCollector\ConfigCollector($config)
            );
            $this->_debugbarRenderer = $this->_debug->getJavascriptRenderer();
            $this->_debug["messages"]->addMessage("hello world!");
        }

        $this->_loadTemplateEngine();
        $this->_init();
    }

	protected function _init() {}

    /**
     * Return Html From Twig
     */
    final protected function render(
        $page, 
        $dataArray = array()
    ) {
        $page  = $this->_theme.$page;
        $debug = array();

        if($this->config->item('DebugBar')) { 
            $debug['_debug_header'] = $this->_debugbarRenderer->renderHead();
            $debug['_debug_footer'] = $this->_debugbarRenderer->render();
        } else {
            $debug['_debug_header'] = '';
            $debug['_debug_footer'] = '';
        }
        $dataArray = array_merge($dataArray, $debug);

        return $this->_twig->render(
            $page,
            $dataArray
        );
    }

    final private function _loadTemplateEngine()
    {
        $this->load->library('Twig');

        $globals     = $this->twig->globals();
        $loader      = new \Twig_Loader_Filesystem(APPPATH.'views');
        $this->_twig = new \Twig_Environment($loader);
        $this->_twig->addGlobal('_GLOBAL', $globals);
        return $this;
    }
}
