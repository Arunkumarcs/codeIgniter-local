<?php
require_once __DIR__.'/Controller.php';

class Api extends Controller 
{    
	public function __construct()
	{
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
		$this->_init();
    }
    
	protected function _init() {}
}
