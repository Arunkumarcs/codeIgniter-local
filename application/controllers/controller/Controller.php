<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller extends CI_Controller 
{
    /**
     * Return JSON Output
     */
	final protected function renderJson($data = array()) 
	{
		header('Content-Type: application/json');
		echo json_encode($data);die;
	}
}
