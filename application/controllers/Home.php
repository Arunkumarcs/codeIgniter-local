<?php
require_once __DIR__.'/controller/Web.php';

class Home extends Web 
{	
	public function index()
	{
		echo $this->render('home.twig');
	}
}
