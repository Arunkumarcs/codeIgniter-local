<?php
require_once __DIR__.'/controller/Web.php';

class Test extends Web 
{
    private $testItems = array(
        'test_name', 
        'test_datatype',
        'res_datatype',
        'result',
        'file',
        'line',
        'notes'
    );
    private $tableTemplate = '
        <table class="table table-striped table-bordered table-hover table-sm">
            <thead class="thead-light">
                <tr>
                <th scope="col">Item</th>
                <th scope="col">Result</th>
                </tr>
            </thead>
            <tbody>
                {rows}
                    <tr>
                        <td>{item}</td>
                        <td>{result}</td>
                    </tr>
                {/rows}
            </tbody>
        </table>';

	public function __construct()
	{
        parent::__construct();
        $this->load->library('unit_test');
        $this->unit->set_template($this->tableTemplate);
        $this->unit->set_test_items($this->testItems);
    }

    public function index() 
    {
        $this->test1()->test2();

		echo $this->render('test.twig', array(
            'body' => $this->unit->report()
        ));
    }

    private function test1()
    {
        $test = 1 + 1;        
        $expected_result = 2;        
        $test_name = 'Adds one plus one';  
        $notes = '';
        $this->unit->run($test, $expected_result, $test_name, $notes);     
        return $this;         
    }
    
    private function test2()
    {
        $test = 1 + 2;        
        $expected_result = 2;        
        $test_name = 'Addsas one plus one';      
        $notes = '';
        $this->unit->run($test, $expected_result, $test_name, $notes);     
        return $this;         
    }
}